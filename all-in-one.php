<?php

// Settings
$scheme = 'facebook';
$ios_scheme = 'fb';
$ios_id = 284882215;
$android_package = 'com.facebook.katana';
$auto = false;

// No trailing slash after path, conform to http://x-callback-url.com/specifications/
$REQUEST_URI = preg_replace('@/(?:\?|$)@', '', $_SERVER['REQUEST_URI']);

// Detection
$HTTP_USER_AGENT = strtolower($_SERVER['HTTP_USER_AGENT']);
$android = (bool) strpos($HTTP_USER_AGENT, 'android');
$iphone = ! $android && ((bool) strpos($HTTP_USER_AGENT, 'iphone') || (bool) strpos($HTTP_USER_AGENT, 'ipod'));
$ipad = ! $android && ! $iphone && (bool) strpos($HTTP_USER_AGENT, 'ipad');
$ios = $iphone || $ipad;
$mobile = $android || $ios;

// Install
$ios_install = 'http://itunes.apple.com/app/id'.$ios_id;
$android_install = 'http://play.google.com/store/apps/details?id='.$android_package;

// Open
if ($ios) {
    $open = $ios_scheme.':/'.$REQUEST_URI;
}
if ($android) {
    $open = 'intent:/'.$REQUEST_URI.'#Intent;package='.$android_package.';scheme='.$scheme.';launchFlags=268435456;end;';
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>URL Schemes</title>
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-itunes-app" content="app-id=284882215, app-argument=fb://view?id=284882215"/>
        <?php if ($ios): ?>
            <meta name="apple-itunes-app" content="app-id=<?php echo $ios_id ?>, app-argument=<?php echo $open ?>"/>
        <?php endif ?>
    </head>
    <body>

        <script>
        function open() {
            window.location = '<?php echo $open ?>';

            <?php if ($ios): ?>
                setTimeout(function() {
                    if (!document.webkitHidden) {
                        window.location = '<?php echo $ios_install ?>';
                    }
                }, 25);
            <?php endif ?>
        }
        </script>

        <?php if ($mobile): ?>
    
            <?php if ($ios): ?>
                <p>Click the banner on top of this screen to <a href="<?php echo $ios_install ?>">install</a> our app or directly <a href="<?php echo $open ?>">open</a> this content in our app if you have it installed already.</p>
    
            <?php elseif ($android): ?>
                <p>Go ahead and <a href="<?php echo $android_install ?>">install</a> our app or directly <a href="<?php echo $open ?>">open</a> this content in our app if you have it installed already.<p>
            <?php endif ?>

            <?php if ($auto): ?>
                <script>open();</script>
            <?php endif ?>

        <?php else: ?>
            <p>Go to the <a href="<?php echo $ios_install ?>">App Store</a> or <a href="<?php echo $android_install ?>">Google Play</a> to install and open this content in our app.</p>
        <?php endif ?>

    </body>
</html>